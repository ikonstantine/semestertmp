package ru.omsu.imit.sem1.trainee;

public class TraineeException extends Exception {

    public TraineeException() {
        super();
    }

    public TraineeException(TraineeErrorCodes tec) {
        super(tec.toString());
    }

    public TraineeException(String str) {
        super(str);
    }

    public TraineeException(int code) {
        // TraineeErrorCodes e = new TraineeErrorCodes(code, "some");

    }
}
