package ru.omsu.imit.sem1.trainee;

import com.google.gson.Gson;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class TraineeProcessor {

    public static ByteBuffer serializationTraineeToByteBuffer(Trainee trainee) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(trainee);
        }
        ByteBuffer bb = ByteBuffer.allocate(baos.size());
        bb.wrap(baos.toByteArray());
        return bb;
    }

    public static Trainee readBytesByMappedByteBuffer(String path) throws TraineeException, IOException{
        String line = "";
        try (FileChannel fc = new RandomAccessFile(new File(path), "r").getChannel()) {
            MappedByteBuffer mBuffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            while (mBuffer.position() < mBuffer.capacity() - 4)
                line += new String(new byte[] {mBuffer.get(), mBuffer.get()});
            line += mBuffer.getInt();
        }
        return stringToTrainee(line);
    }

    private static Trainee stringToTrainee(String string) throws TraineeException {
        Trainee trainee = new Trainee();
        int[] indexs = new int[2];
        int index = 0;
        char[] array = string.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (Character.isUpperCase(array[i])) {
                indexs[index] = i;
                index++;
            }
        }
        trainee.setFirstName(string.substring(indexs[0], indexs[1]));
        trainee.setLastName(string.substring(indexs[1], string.length() - 1));
        trainee.setRating(Integer.parseInt(string.substring(string.length() - 1)));
        return trainee;
    }

    public static Trainee readBytesByByteBuffer(String path) throws IOException, TraineeException{

        ByteBuffer byteBuffer = null;
        try (FileChannel fc = new FileInputStream(new File(path)).getChannel()) {
            byteBuffer = ByteBuffer.allocate((int) fc.size());
            fc.read(byteBuffer);
        }
        byteBuffer.position(0);
        Trainee t = new Trainee();
        String line = "";
        while (byteBuffer.position() < byteBuffer.capacity() - 4)
            line += new String(new byte[] {byteBuffer.get(), byteBuffer.get()});
        line += byteBuffer.getInt();
        return stringToTrainee(line);
    }

    public static void writeBytesToFile(String path, Trainee trainee) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(new File(path)))) {
            dos.writeChars(trainee.getFirstName());
            dos.writeChars(trainee.getLastName());
            dos.writeInt(trainee.getRating());
        }
    }

    public static void serializeToGsonFile(Trainee trainee, String path) throws IOException {
        Gson gson = new Gson();
        String gsonString = gson.toJson(trainee);
        writeTrainee(path, gsonString);
    }

    public static Trainee deserializeFromJsonFileToTrainee(String path) throws IOException {
        Trainee traine = new Trainee();
        Gson gson = new Gson();
        try (FileReader fr = new FileReader(path)){
            traine = gson.fromJson(fr, Trainee.class);
        }
        return traine;
    }

    public static Trainee serializeToGsonAndDeserializeToTrainee(Trainee trainee) {
        Gson gson = new Gson();
        String gsonString = gson.toJson(trainee);
        return gson.fromJson(gsonString, Trainee.class);
    }

    public static void serializeTrainee(Trainee trainee, String path) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeToTrainee(String path) throws IOException, ClassNotFoundException {
        Trainee trainee;
        try (ObjectInputStream oin = new ObjectInputStream(new FileInputStream(path))) {
            trainee = (Trainee) oin.readObject();
        }
        return trainee;
    }

    public static String contentInThreeLines(Trainee trainee) {
        return String.format("%s\r\n%s\r\n%d", trainee.getFirstName(), trainee.getLastName(), trainee.getRating());
    }

    public static Trainee readTraineeByRegEx(String path) throws TraineeException, IOException {
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(path))){
            line = br.readLine();
        }
        return stringToTrainee(line);
    }

    public static Trainee readTraineeFromFile(String path) throws TraineeException {
        Trainee trainee = new Trainee();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(path))){
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        String[] words = line.split(" ");
        trainee.setFirstName(words[0]);
        trainee.setLastName(words[1]);
        trainee.setRating(Integer.parseInt(words[2]));
        return trainee;
    }

    public static void writeTrainee(String path, String content) throws IOException {
        try (FileWriter fileWriter = new FileWriter(path, false)){
            fileWriter.write(content);
        }
    }
}
