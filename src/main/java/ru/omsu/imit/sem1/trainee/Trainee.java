package ru.omsu.imit.sem1.trainee;

import java.io.Serializable;

public class Trainee extends TraineeException implements Serializable, Comparable<Trainee> {

    private String firstName;
    private String lastName;
    private int rating;

    public Trainee() {
        firstName = "Firstname";
        lastName = "Lastname";
        rating = 0;
    }

    public Trainee(String firstName, String lastName, int rating) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setRating(rating);
    }

    public void setFirstName(String firstName) throws TraineeException {
        if (firstName != null && !firstName.equals("")) this.firstName = firstName;
        else throw new TraineeException(TraineeErrorCodes.TRAINEE_ERROR_NAME);
    }

    public void setLastName(String lastName) throws TraineeException {
        if (lastName != null && !lastName.equals("")) this.lastName = lastName;
        else throw new TraineeException(TraineeErrorCodes.TRAINEE_ERROR_NAME);
    }

    public void setRating(int rating) throws TraineeException{
        if (rating >= 1 && rating <= 5) this.rating = rating;
        else throw new TraineeException(TraineeErrorCodes.TRAINEE_ERROR_RATING_RANGE);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != this.getClass()) return false;
        if (o == null) return false;
        if (o == this) return true;
        Trainee tmp = (Trainee) o;
        return (tmp.getFirstName().equals(((Trainee) o).getFirstName()) &&
                tmp.getLastName().equals(((Trainee) o).getLastName()) &&
                tmp.getRating() == ((Trainee) o).getRating());
    }

    @Override
    public int hashCode() {
        return 27 * firstName.hashCode() * lastName.hashCode() * getRating();
    }

    @Override
    public String toString() {
        return new String().format("[%s, %s, %d]", getLastName(),getFirstName(),getRating());
    }

    @Override
    public int compareTo(Trainee trainee) {
        int res = getLastName().compareTo(trainee.getLastName());
        return (res != 0) ? res : getFirstName().compareTo(trainee.getFirstName());
    }
}
