package ru.omsu.imit.sem1.trainee;

public enum TraineeErrorCodes {
    TRAINEE_ERROR_NAME(0, "first and last name must not be null or empty"),
    TRAINEE_ERROR_RATING_RANGE(1, "range of rating must be from 1 to 5");

    private final int code;
    private final String reason;

    private TraineeErrorCodes(int code, String reason) {
        this.code = code;
        this.reason = reason;
    }
    public String getReason() {
        return reason;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "ERROR(" + code + "): " + reason;
    }
}
