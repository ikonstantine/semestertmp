package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
    public static void main(String[] args) throws TraineeException {
        Set<Trainee> trainees = new TreeSet<>();
        trainees.add(new Trainee());
        Trainee t = new Trainee("Jet", "Brains", 5);
        Trainee tt = new Trainee("Vasya", "Pumpkin", 5);
        trainees.add(t);
        trainees.add(tt);
        System.out.println(trainees.contains(t));
        System.out.println(trainees.contains(new Trainee("User", "Name", 5)));
        System.out.println(trainees);

    }
}
