package ru.omsu.imit.sem1.collectionsdemo;

import java.util.EnumSet;

enum Color {
    RED, GREEN, BLUE
}
public class EnumSetDemo {
    public static void main(String[] args) {
        EnumSet<Color> enumSetAllOf = EnumSet.allOf(Color.class);
        EnumSet<Color> enumSetJustOne = EnumSet.of(Color.BLUE);
        EnumSet<Color> enumSetRange = EnumSet.range(Color.RED, Color.BLUE);
        EnumSet<Color> enumSetNone = EnumSet.noneOf(Color.class);

        System.out.println(enumSetAllOf.contains(Color.GREEN)); // true
        System.out.println(enumSetJustOne.contains(Color.GREEN)); // false
        System.out.println(enumSetRange.contains(Color.GREEN)); // true
        System.out.println(enumSetNone.contains(Color.GREEN)); // false
    }
}
