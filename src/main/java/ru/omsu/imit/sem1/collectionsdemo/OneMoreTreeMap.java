package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.institute.Institute;
import ru.omsu.imit.sem1.institute.InstituteException;
import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class OneMoreTreeMap {
    public static void main(String[] args) throws TraineeException, InstituteException {
        Map<Trainee, Institute> traineeInstituteMap = new TreeMap<>(new Comparator<Trainee>() {
            @Override
            public int compare(Trainee t1, Trainee t2) {
                int res = t1.getLastName().compareTo(t2.getLastName());
                return (res != 0) ? res : t1.getFirstName().compareTo(t2.getFirstName());
            }
        });
        Trainee trainee1 = new Trainee("Vitya", "Vdovichenko", 5);
        Trainee trainee2 = new Trainee("Anton", "Zaycev", 5);
        Trainee trainee3 = new Trainee("Misha", "Kuc", 5);
        Trainee almostSameTrainee = new Trainee("Vitya", "Nevdovichenko", 5);
        Institute instituteOmSU = new Institute("Omsk State University", "Omsk");

        traineeInstituteMap.put(trainee1, instituteOmSU);
        traineeInstituteMap.put(trainee2, instituteOmSU);
        traineeInstituteMap.put(trainee3, instituteOmSU);

        System.out.println(trainee1 + " " + traineeInstituteMap.get(trainee1)); // exist
        System.out.println(almostSameTrainee + " " + traineeInstituteMap.get(almostSameTrainee)); // not exist, than null

        for (Trainee trainee : traineeInstituteMap.keySet())
            System.out.println(trainee);
        for (Map.Entry<Trainee, Institute> entry : traineeInstituteMap.entrySet())
            System.out.println(entry.getKey() + " " + entry.getValue());
    }
}
