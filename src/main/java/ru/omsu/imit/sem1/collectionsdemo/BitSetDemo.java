package ru.omsu.imit.sem1.collectionsdemo;

import java.util.BitSet;

public class BitSetDemo {
    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        bitSet.set(0, 10);
        bitSet.set(15, 18);
        bitSet.clear(16);
        for (int i = 0; i < 20; i++)
            System.out.println(i + ") " + bitSet.get(i));
    }
}
