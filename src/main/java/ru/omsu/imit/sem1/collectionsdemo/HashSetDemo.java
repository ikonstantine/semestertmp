package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {
    public static void main(String[] args) throws TraineeException {
        Set<Trainee> trainees = new HashSet<>();
        Trainee t = new Trainee("Jet", "Brains", 5);
        Trainee tt = new Trainee("Vasya", "Pumpkin", 5);

        trainees.add(new Trainee());
        trainees.add(t);

        System.out.println(trainees.contains(t));
        System.out.println(trainees.contains(tt));

        System.out.println(trainees);
    }
}
