package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.institute.Institute;
import ru.omsu.imit.sem1.institute.InstituteException;
import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.HashMap;
import java.util.Map;

public class HashMapTI {
    public static void main(String[] args) throws TraineeException, InstituteException {
        Map<Trainee, Institute> traineeInstituteMap = new HashMap<>();

        Trainee trainee1 = new Trainee("Vitya", "Vdovichenko", 5);
        Trainee trainee2 = new Trainee("Anton", "Zaycev", 5);
        Trainee trainee3 = new Trainee("Misha", "Kuc", 5);

        Institute instituteOmSU = new Institute("Omsk State University", "Omsk");

        traineeInstituteMap.put(trainee1, instituteOmSU);
        traineeInstituteMap.put(trainee2, instituteOmSU);
        traineeInstituteMap.put(trainee3, instituteOmSU);

        Institute lostInstitute = traineeInstituteMap.get(trainee1);
        System.out.println(lostInstitute.getName());

        for (Trainee trainee : traineeInstituteMap.keySet())
            System.out.println(trainee);
    }
}
