package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.*;

/**
 * Created by Constantine on 13.01.2018.
 */
public class ArrayListAndTrainee {
    public static void main(String[] args) throws TraineeException {
        List<Trainee> arrayListOfTrainees = new LinkedList<>();
        arrayListOfTrainees.add(new Trainee("Bruce", "Eckel", 5));
        arrayListOfTrainees.add(new Trainee("Jango", "Texas", 1));
        arrayListOfTrainees.add(new Trainee("Carl", "Durrel", 2));
        arrayListOfTrainees.add(new Trainee("John", "Fury", 3));
        arrayListOfTrainees.add(new Trainee("Abraham", "Reedus", 4));
        arrayListOfTrainees.add(new Trainee("Gerald", "Catswill", 5));

        printList(arrayListOfTrainees);

        Collections.reverse(arrayListOfTrainees);
        System.out.println("Reverse:");
        printList(arrayListOfTrainees);

        Collections.rotate(arrayListOfTrainees, 2);
        System.out.println("Shift:");
        printList(arrayListOfTrainees);

        Collections.shuffle(arrayListOfTrainees);
        System.out.println("Shuffle:");
        printList(arrayListOfTrainees);

        int maxRating = 1;
        for (Trainee trainee : arrayListOfTrainees)
            if (trainee.getRating() > maxRating) maxRating = trainee.getRating();
        System.out.println("Max rating: " + maxRating + "\n");

        Collections.sort(arrayListOfTrainees);
        System.out.println("Sort by name:");
        printList(arrayListOfTrainees);

        Collections.sort(arrayListOfTrainees, new Comparator<Trainee>() {
            @Override
            public int compare(Trainee t1, Trainee t2) {
                return t1.getRating() - t2.getRating();
            }
        });
        System.out.println("Sort by rating:");
        printList(arrayListOfTrainees);


        System.out.println("Exist: " + arrayListOfTrainees.contains(new Trainee("Bruce", "Eckel", 5)));

    }

    public static void printList(List<Trainee> list) {
        for (Trainee trainee : list)
            System.out.println(trainee);
        System.out.println();
    }
}
