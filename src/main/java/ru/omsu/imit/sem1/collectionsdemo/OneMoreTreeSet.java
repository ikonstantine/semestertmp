package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.Comparator;
import java.util.TreeSet;

public class OneMoreTreeSet {

    public static void main(String[] args) throws TraineeException {
        TreeSet<Trainee> trainees = new TreeSet<>(new Comparator<Trainee>() {
            @Override
            public int compare(Trainee t1, Trainee t2) {
                int res = t1.getLastName().compareTo(t2.getLastName());
                return (res != 0) ? res : t1.getFirstName().compareTo(t2.getFirstName());
            }
        });

        Trainee a = new Trainee("Anton", "Antonov", 5);
        trainees.add(a);

        System.out.println(trainees.contains(a));//true
        System.out.println(trainees.contains(new Trainee("Anton", "petrovich", 5)));//false
        System.out.println(trainees);
    }
}
