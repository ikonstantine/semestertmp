package ru.omsu.imit.sem1.collectionsdemo;

import ru.omsu.imit.sem1.trainee.Trainee;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {
    public static void main(String[] args) {
        Queue<Trainee> trainees = new LinkedList<>();
        trainees.add(new Trainee());
        trainees.add(new Trainee());
        trainees.add(new Trainee());
        trainees.add(new Trainee());
        trainees.add(new Trainee());
        System.out.println("isEmpty(): " + trainees.isEmpty());
        while (!trainees.isEmpty())
            System.out.println(trainees.poll());
        System.out.println("isEmpty(): " + trainees.isEmpty());
    }
}
