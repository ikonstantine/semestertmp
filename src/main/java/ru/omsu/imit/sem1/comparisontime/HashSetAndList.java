package ru.omsu.imit.sem1.comparisontime;

import java.util.*;

public class HashSetAndList {
    public static void main(String[] args) {
        List<Integer> traineesList = new ArrayList<>();
        Set<Integer> traineeHashSet = new HashSet<>();
        Set<Integer> traineesTreeSet = new TreeSet<>();

        Random random = new Random();
        int randomValue;
        int size = 100000;
        int range = 5000;
        for (int i = 0; i < size; i++) {
            randomValue = random.nextInt(range);
            traineesList.add(randomValue);
            traineeHashSet.add(randomValue);
            traineesTreeSet.add(randomValue);
        }

        //comparison
        int someRandomValue= random.nextInt(range);

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            traineesList.contains(someRandomValue);
        }
        long time = System.currentTimeMillis() - startTime;
        System.out.println("Time List: " + time + " ms"); // write time

        startTime = System.currentTimeMillis();
        for (int i =0; i < 10000; i++)
            traineeHashSet.contains(someRandomValue);
        time = System.currentTimeMillis() - startTime;
        System.out.println("Time HashSet: " + time + " ms");

        startTime = System.currentTimeMillis();
        for (int i =0; i < 10000; i++)
            traineesTreeSet.contains(someRandomValue);
        time = System.currentTimeMillis() - startTime;
        System.out.println("Time TreeSet: " + time + " ms");
    }
}
