package ru.omsu.imit.sem1.comparisontime;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashTreeBitSets {
    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        Set<Integer> hashSet = new HashSet<>();
        Set<Integer> treeSet = new TreeSet<>();
        int times = 1000000;

        long startTime = System.currentTimeMillis();
        bitSet.set(0, times);
        long finishTime = System.currentTimeMillis() - startTime;
        System.out.println("bitSet " + finishTime + " ms"); // almost 0 ms

        startTime = System.currentTimeMillis();
        for (int i = 0; i < times; i++)
            hashSet.add(i);
        finishTime = System.currentTimeMillis() - startTime;
        System.out.println("hashSet " + finishTime + " ms"); // around 400-500 ms

        startTime = System.currentTimeMillis();
        for (int i = 0; i < times; i++)
            treeSet.add(i);
        finishTime = System.currentTimeMillis() - startTime;
        System.out.println("treeSet " + finishTime + " ms"); // around 800-900 ms
    }
}
