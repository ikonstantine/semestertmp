package ru.omsu.imit.sem1.comparisontime;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Constantine on 14.01.2018.
 */
public class LinkedListAndArrayList {

    public static void main(String[] args) {
        speedComparisonOfLinkedListAndArrayList();
    }

    public static void speedComparisonOfLinkedListAndArrayList() {
        System.out.println("ArrayList and LinkedList:");
        int size = 100000;
        List<Integer> linkedList = new LinkedList<>();
        List<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            linkedList.add(0);
            arrayList.add(0);
        }

        Random random = new Random();
        long timeOfStart = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            linkedList.get(random.nextInt(size));
        }
        long differenceTime = System.currentTimeMillis() - timeOfStart;
        System.out.println(differenceTime + " ms (linkedList)"); // result: 4000 - 4200 ms

        timeOfStart = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            arrayList.get(random.nextInt(size));
        }
        differenceTime = System.currentTimeMillis() - timeOfStart;
        System.out.println(differenceTime + " ms (arrayList)"); // result: 1 - 18 ms
    }
}
