package ru.omsu.imit.sem1.institute;

public enum InstituteErrorCodes {
    INSTITUTE_ERROR_NAME(0, "name of institute must not be null or empty"),
    INSTITUTE_ERROR_NAMEOFCITY(1, "name of city must not be null or empty");

    int code;
    String reason;

    InstituteErrorCodes(int code, String reason) {
        this.code = code;
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ERROR(" + code + "): " + reason;
    }
}
