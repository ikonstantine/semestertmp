package ru.omsu.imit.sem1.institute;

/**
 * Created by Constantine on 12.01.2018.
 */
public class InstituteException extends Exception {
    public InstituteException() {
        super();
    }

    public InstituteException(String str) {
        super(str);
    }

    public InstituteException(InstituteErrorCodes iec) {
        super(iec.toString());
    }
}
