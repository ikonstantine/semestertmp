package ru.omsu.imit.sem1.institute;

public class Institute {

    private String name;
    private String nameOfCity;

    public Institute (String name, String nameOfCity) throws InstituteException {
        setName(name);
        setNameOfCity(nameOfCity);
    }

    public String getName() {
        return name;
    }

    public String getNameOfCity(){
        return nameOfCity;
    }

    public void setName(String name) throws InstituteException {
        if (name != null && !name.equals("") && !name.equals(" ")) this.name = name;
        else throw new InstituteException(InstituteErrorCodes.INSTITUTE_ERROR_NAME);
    }

    public void setNameOfCity(String nameOfCity) throws InstituteException {
        if (nameOfCity != null && !nameOfCity.equals("") && !nameOfCity.equals(" ")) this.nameOfCity = nameOfCity;
        else throw new InstituteException(InstituteErrorCodes.INSTITUTE_ERROR_NAMEOFCITY);
    }

}
