package ru.omsu.imit.sem1.rectangle;

import java.io.*;

public class ProcessOfRectangle {
    public static void writeToBinaryFile(Rectangle rectangle, String path) {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(path))){
            dos.writeInt(rectangle.getLeft());
            dos.writeInt(rectangle.getTop());
            dos.writeInt(rectangle.getRight());
            dos.writeInt(rectangle.getBottom());
            dos.flush();
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Rectangle readFromBinaryFile(String path) {
        Rectangle rectangle = new Rectangle();
        try (DataInputStream dos = new DataInputStream(new FileInputStream(path))) {
            rectangle.setLeft(dos.readInt());
            rectangle.setTop(dos.readInt());
            rectangle.setRight(dos.readInt());
            rectangle.setBottom(dos.readInt());
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return rectangle;
    }

    //////////////!!!!!!!!!!!!!!!!!!!!               10
    public static Rectangle[] reverseReadFromBinaryFile(String path, int amount) {
        Rectangle[] rectangles = new Rectangle[amount];
        try (DataInputStream dis = new DataInputStream(new FileInputStream(path))){
            byte[] buffer = new byte[dis.available()];
            dis.read(buffer, 0, dis.available());

        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return rectangles;
    }
}
