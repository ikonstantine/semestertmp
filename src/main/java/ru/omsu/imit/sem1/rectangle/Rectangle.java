package ru.omsu.imit.sem1.rectangle;

import ru.omsu.imit.sem1.point.Point;

public class Rectangle {
    private Point leftTop;
    private Point rightBottom;

    public Rectangle() {
        leftTop = new Point(0, 0);
        rightBottom = new Point(1, 1);
    }

    public Rectangle(int left, int top, int right, int bottom) {
        leftTop = new Point(left, top);
        rightBottom = new Point(right, bottom);
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != this.getClass()) return false;
        if (o == null) return false;
        if (o == this) return true;
        Rectangle tmp = (Rectangle) o;
        return (getLeft() == tmp.getLeft() && getTop() == tmp.getTop() && getRight() == tmp.getRight() && getBottom() == tmp.getBottom());
    }

    @Override
    public String toString() {
        return String.format("leftTop (%d, %d); rightBottom (%d, %d);", leftTop.getX(), leftTop.getY(), rightBottom.getX(), rightBottom.getY());
    }

    public int getLeft() {
        return leftTop.getX();
    }

    public int getRight() {
        return rightBottom.getX();
    }

    public int getTop() {
        return leftTop.getY();
    }

    public int getBottom() {
        return rightBottom.getY();
    }

    public void setLeft(int left) {
        leftTop.setX(left);
    }

    public void setRight(int right) {
        rightBottom.setX(right);
    }

    public void setTop(int top) {
        leftTop.setY(top);
    }

    public void setBottom(int bottom) {
        rightBottom.setY(bottom);
    }
}

