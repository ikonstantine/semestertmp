package ru.omsu.imit.sem1.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

class ThreadWhichAddElemToList implements Runnable {
    Thread t;
    private List<Integer> list;
    private ReentrantLock lock;

    public ThreadWhichAddElemToList(List<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
        this.t = new Thread(this, "adder");
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            try {
                list.add(new Random().nextInt());
                System.out.println("Added one integer");
            } finally {
                lock.unlock();
            }
        }
    }
}

class ThreadWhichRemoveFromList implements Runnable {
    Thread t;
    private List<Integer> list;
    private ReentrantLock lock;

    public ThreadWhichRemoveFromList(List<Integer> list, ReentrantLock lock) {
        this.list = list;
        this.lock = lock;
        this.t = new Thread(this, "remover");
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            try {
                if (!list.isEmpty()) {
                    list.remove(new Random().nextInt(list.size()));
                    System.out.println("Removed one integer");
                }
            } finally {
                lock.unlock();
            }
        }
    }
}
public class AddAndRemoveInArrayListWithLock {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        ThreadWhichAddElemToList add = new ThreadWhichAddElemToList(list, lock);
        ThreadWhichRemoveFromList rmv = new ThreadWhichRemoveFromList(list, lock);

        try {
            add.t.join();
            rmv.t.join();
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }
}
