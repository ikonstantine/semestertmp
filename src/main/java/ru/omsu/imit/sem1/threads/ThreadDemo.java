package ru.omsu.imit.sem1.threads;

public class ThreadDemo {
    public static void main(String[] args) {
        Thread thread = Thread.currentThread();
        System.out.println("Properties of the current thread: " + thread);
    }
}
