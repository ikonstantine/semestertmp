package ru.omsu.imit.sem1.threads.multitask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Constantine on 08.03.2018.
 */
public class MultiTaskMonitor implements Runnable {
    private BlockingQueue<MultiTask> queue;
    private CountDownLatch cdl;
    private int readers;

    public MultiTaskMonitor(BlockingQueue<MultiTask> queue, CountDownLatch cdl, int readers) {
        this.queue = queue;
        this.cdl = cdl;
        this.readers = readers;
    }

    public void run() {
        List<Executable> list = new ArrayList<>();
        list.add(new Task(-1));
        try {
            cdl.await();
            for (int i = 0; i < readers; i++)
                queue.add(new MultiTask(list));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}