package ru.omsu.imit.sem1.threads.multitask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class MultiTaskProducer implements Runnable {
    private BlockingQueue<MultiTask> queue;

    public MultiTaskProducer(BlockingQueue<MultiTask> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        List<Executable> list = new ArrayList<>();
        for (int i = 0; i < 3; i++)
            list.add(new Task((int) (Math.random() * 10)));
        queue.add(new MultiTask(list));
    }
}
