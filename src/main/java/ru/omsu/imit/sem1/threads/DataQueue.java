package ru.omsu.imit.sem1.threads;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

class Data {
    private int[] array;

    public Data(int[] array) {
        this.array = array;
    }

    public int[] get() {
        return this.array;
    }
}

class DataProducer implements Runnable {

    private BlockingQueue<Data> queue;

    public DataProducer(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    public void run() {
        int[] array = new int[3];
        try {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < array.length; j++)
                    array[j] = (int) (Math.random() * 10);
                queue.put(new Data(array));
                synchronized(System.out) {
                    System.out.println("Producer added: " + Arrays.toString(array));
                }
                Thread.sleep(200);
            }
            array = new int[0];
            queue.put(new Data(array));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class DataConsumer implements Runnable {

    private BlockingQueue<Data> queue;

    public DataConsumer(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    public void run() {
        while (true) {
            try {
                int[] data = queue.take().get();
                if (data.length == 0)
                    break;
                synchronized(System.out) {
                    System.out.println("Consumer retrieved: " + Arrays.toString(data));
                }
                Thread.sleep(200);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}


class SetCommand implements Runnable {
    @Parameter(names = {"-ca"}, arity = 1, description = "Amount of thread-consumer")
    int amountConsumer;

    @Parameter(names = {"-pa"}, arity = 1, description = "Amount of thread-producer")
    int amountProducer;

    public void run() {
        System.out.println(amountConsumer + " " + amountProducer);
        BlockingQueue<Data> queue = new ArrayBlockingQueue<>(100);
        ExecutorService executorService1 = Executors.newFixedThreadPool(5);
        for (int i = 0; i < amountConsumer; i++)
            executorService1.submit(new DataConsumer(queue));
        executorService1.shutdown();

        ExecutorService executorService2 = Executors.newFixedThreadPool(5);
        for (int i = 0; i < amountProducer; i++)
            executorService2.submit(new DataProducer(queue));
        executorService2.shutdown();
    }
}

public class DataQueue {
    public static void main(String[] args) {
        args = new String[] {"-ca", "5", "-pa", "5"};
        SetCommand sc = new SetCommand();
        JCommander.newBuilder().addObject(sc).build().parse(args);
        sc.run();
    }
}
