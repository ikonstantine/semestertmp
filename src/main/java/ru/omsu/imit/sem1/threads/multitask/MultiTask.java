package ru.omsu.imit.sem1.threads.multitask;

import java.util.List;

public class MultiTask {

    private List<Executable> list;

    public MultiTask(List<Executable> list) {
        this.list = list;
    }

    public List<Executable> getList() {
        return list;
    }
}
