package ru.omsu.imit.sem1.threads.multitask;

public interface Executable {
    public void execute();
}
