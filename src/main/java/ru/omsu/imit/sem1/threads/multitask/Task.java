package ru.omsu.imit.sem1.threads.multitask;

public class Task implements Executable {
    private int value;

    public Task(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public void execute() {
        System.out.println("Executed by " + Thread.currentThread().getId() + " , value: " + value);
    }
}
