package ru.omsu.imit.sem1.threads.multitask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class TaskProducer implements Runnable {
    private BlockingQueue<Task> queue;
    private CountDownLatch cdl;

    public TaskProducer(BlockingQueue<Task> queue, CountDownLatch cdl) {
        this.queue = queue;
        this.cdl = cdl;
    }

    @Override
    public void run() {
        int value;
        try {
            for (int i = 0; i < 5; i++) {
                value = (int) (Math.random() * 10);
                queue.add(new Task(value));
                synchronized (System.out) {
                    System.out.println("Producer " + Thread.currentThread().getId() + " added: " + value);
                }
            }
            cdl.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
