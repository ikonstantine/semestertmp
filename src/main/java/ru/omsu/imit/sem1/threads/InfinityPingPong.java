package ru.omsu.imit.sem1.threads;

import java.util.concurrent.Phaser;

public class InfinityPingPong {

    static final Phaser phaser = new Phaser(1);

    public static void main(String[] args) {
        tPingPong("PING");
        tPingPong("pong");
    }

    private static void tPingPong(String string) {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    System.out.println(getName() + " " + string);
                    phaser.awaitAdvance(phaser.arrive() + 1);
                }
            }
        }.start();
    }
}