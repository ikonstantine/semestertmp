package ru.omsu.imit.sem1.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class ThreadWhichAddElemFromArrayList implements Runnable {
    private Thread thread;
    private List<Integer> arrayList;

    ThreadWhichAddElemFromArrayList (String name, List arrayList) {
        thread = new Thread(this, name);
        this.arrayList = arrayList;
        thread.start();
    }

    public void run() {
        Random random = new Random();
        try {
            for (int i = 0; i < 10000; i++) {
                synchronized (arrayList) {
                    arrayList.add(random.nextInt(10000));
                }
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class ThreadWhichDelElemFromArrayList implements Runnable {
    private Thread thread;
    private List<Integer> arrayList;

    ThreadWhichDelElemFromArrayList (String name, List arrayList) {
        thread = new Thread(this, name);
        this.arrayList = arrayList;
        thread.start();
    }

    public void run() {
        Random random = new Random();
        try {
            for (int i = 0; i < 10000; i++) {
                synchronized (arrayList) {
                    if (!arrayList.isEmpty()) {
                        arrayList.remove(random.nextInt(arrayList.size()));
                    }
                }
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class TwoOtherThreadsWithSynchArrayList {
    public static void main(String[] args) {
        Thread mainThread = Thread.currentThread();
        List arrayList = new ArrayList<Integer>();

        new ThreadWhichAddElemFromArrayList("add", arrayList);
        new ThreadWhichDelElemFromArrayList("del", arrayList);
    }
}
