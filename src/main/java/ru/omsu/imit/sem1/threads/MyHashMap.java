package ru.omsu.imit.sem1.threads;

import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyHashMap {
    
    private Map<Integer, String> map;

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public MyHashMap(Map<Integer, String> map) {
        this.map = map;
    }

    public Map<Integer, String> getMap() {
        Map<Integer, String> res;
        lock.readLock().lock();
        try {
            res = map;
        } finally {
            lock.readLock().unlock();
        }
        return map;
    }

    public void setMap(Map<Integer, String> map) {
        lock.writeLock().lock();
        try {
            this.map = map;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void put(int x, String str) {
        lock.writeLock().lock();
        try {
            this.map.put(x, str);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public String get(int x) {
        String res;
        lock.readLock().lock();
        try {
            res = this.map.get(x);
        } finally {
            lock.readLock().unlock();
        }
        return res;
    }

    public boolean containsKey(int x) {
        boolean res;
        lock.readLock().lock();
        try {
            res = this.map.containsKey(x);
        } finally {
            lock.readLock().unlock();
        }
        return res;
    }

    public void remove(int x) {
        lock.writeLock().lock();
        try {
            this.map.remove(x);
        }finally {
            lock.writeLock().unlock();
        }
    }

    public void clear() {
        lock.writeLock().lock();
        try {
            this.map.clear();
        } finally {
            lock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        boolean res;
        lock.readLock().lock();
        try {
            res = this.map.isEmpty();
        } finally {
            lock.readLock().unlock();
        }
        return res;
    }
}
