package ru.omsu.imit.sem1.threads.multitask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class MultiTaskConsumer implements Runnable {
    private BlockingQueue<MultiTask> queue;
    private CountDownLatch cdl;

    public MultiTaskConsumer(BlockingQueue<MultiTask> queue, CountDownLatch cdl) {
        this.queue = queue;
        this.cdl = cdl;
    }

    @Override
    public void run() {
        MultiTask mt;
        Task t;
        while (true) {
            try {
                mt = queue.take();
                t = (Task) mt.getList().remove(0);
                if (t.getValue() == -1)
                    break;
                t.execute();
                if (!mt.getList().isEmpty())
                    queue.add(mt);
                else
                    cdl.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
