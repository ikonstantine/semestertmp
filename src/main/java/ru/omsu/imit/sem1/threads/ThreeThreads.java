package ru.omsu.imit.sem1.threads;

public class ThreeThreads {
    public static void main(String[] args) {
        NewThread1 newThread1 = new NewThread1("A");
        NewThread2 newThread2 = new NewThread2("B");
        NewThread3 newThread3 = new NewThread3("C");
        try {
            newThread1.thread.join();
            newThread2.thread.join();
            newThread3.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread end");
    }
}

class NewThread3 implements Runnable {
    Thread thread;

    NewThread3 (String name) {
        thread = new Thread(this, name);
        System.out.println("Properties of the child thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " interrupted.");
        }
        System.out.println("End " + Thread.currentThread().getName());
    }
}

class NewThread2 implements Runnable {
    Thread thread;

    NewThread2 (String name) {
        thread = new Thread(this, name);
        System.out.println("Properties of the child thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " interrupted.");
        }
        System.out.println("End " + Thread.currentThread().getName());
    }
}

class NewThread1 implements Runnable {
    Thread thread;

    NewThread1 (String name) {
        thread = new Thread(this, name);
        System.out.println("Properties of the child thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " interrupted.");
        }
        System.out.println("End " + Thread.currentThread().getName());
    }
}