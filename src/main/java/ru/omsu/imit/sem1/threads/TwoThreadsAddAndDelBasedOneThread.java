package ru.omsu.imit.sem1.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class ThreadAddAndDel implements Runnable {
    private Thread thread;
    private List<Integer> arrayList;

    ThreadAddAndDel (String name, List arrayList) {
        thread = new Thread(this, name);
        this.arrayList = arrayList;
        thread.start();
    }

    public void run() {
        addElem();
        removeElem();
    }

    public void addElem() {
        for (int i = 0; i < 10000; i++) {
            arrayList.add(new Random().nextInt(10000));
            System.out.println("Added one integer");
        }
    }

    public void removeElem() {
        for (int i = 0; i < 10000; i++) {
            if (!arrayList.isEmpty()) {
                arrayList.remove(new Random().nextInt(arrayList.size()));
                System.out.println("Removed one integer");
            }
        }
    }
}

public class TwoThreadsAddAndDelBasedOneThread {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        new ThreadAddAndDel("AdderAndRemoverThread", list);
    }
}
