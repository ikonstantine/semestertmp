package ru.omsu.imit.sem1.threads;

public class WhenChildThreadEnd {
    public static void main(String[] args) {
        NewThread nT = new NewThread("ChildThread");
        try {
            nT.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread end");
    }
}
