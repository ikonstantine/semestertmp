package ru.omsu.imit.sem1.threads;

public class NewThread implements Runnable {
    Thread thread;

    NewThread (String name) {
        thread = new Thread(this, name);
        System.out.println("Properties of the child thread: " + thread);
        thread.start();
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(Thread.currentThread().getName() + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " interrupted.");
        }
        System.out.println("End " + Thread.currentThread().getName());
    }
}
