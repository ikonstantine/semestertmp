package ru.omsu.imit.sem1.threads;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Message {
    private String emailAddress;
    private String sender;
    private String subject;
    private String body;

    public Message(String emailAddress, String sender, String subject, String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    @Override
    public String toString() {
        return "From: " + sender + "; To: " + emailAddress + "; Subject: " + subject + "; Body: " + body + ";\n";
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getSender() {
        return sender;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

}

public class MessageSender {

    private final static String SENDER_EMAIL = "ikonstantine@.ru";

    public static void main(String args[]) throws FileNotFoundException {
        ExecutorService es = Executors.newFixedThreadPool(5);
        BufferedReader br = new BufferedReader(new FileReader("emails.txt"));
        for (int i = 0; i < 20; i++)
            es.submit(new Runnable() {
                @Override
                public void run() {
                    String email;
                    try {
                        email = br.readLine();
                        if (email != null)
                            send(new Message(email, SENDER_EMAIL, "Testing", "Hello world!"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        es.shutdown();
    }


    public static void send(Message msg) throws IOException {
        try (FileWriter fw = new FileWriter("output.txt", true)) {
            fw.write(msg.toString());
        }
    }
}