package ru.omsu.imit.sem1.threads;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class SystemReaderWriter {
    static private int i = 0;
    static private ReadWriteLock lock = new ReentrantReadWriteLock();

    public static void main(String[] args) {
        Runnable write = () -> {
            lock.writeLock().lock();
            System.out.println("  Write lock locked by thread " + Thread.currentThread().getId());
            try {
                i++;
                System.out.println("  Writer thread " + Thread.currentThread().getId() + " set value " + i);
            } finally {
                System.out.println("  Write lock unlocked by thread " + Thread.currentThread().getId());
                lock.writeLock().unlock();
            }
        };

        Runnable read = () -> {
            lock.readLock().lock();
            System.out.println("Read lock locked by thread " + Thread.currentThread().getId());
            try {
                System.out.println("Reader thread " + Thread.currentThread().getId() + " read value " + i);
            } finally {
                System.out.println("Read lock unlocked by thread " + Thread.currentThread().getId());
                lock.readLock().unlock();
            }
        };

        new Thread(read).start();
        new Thread(read).start();
        new Thread(read).start();
        new Thread(write).start();
        new Thread(read).start();
    }

}
