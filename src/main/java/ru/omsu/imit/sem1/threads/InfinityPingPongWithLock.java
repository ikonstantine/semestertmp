package ru.omsu.imit.sem1.threads;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class PingPongLock {
    private Lock lock = new ReentrantLock();
    private Condition pinged = lock.newCondition();
    private Condition ponged = lock.newCondition();
    private String str = "";

    public void ping() throws InterruptedException {
        lock.lock();
        try {
            while(str == "PING")
                ponged.await();
            str = "PING";
            pinged.signal();
            System.out.println(str);
        } finally {
            lock.unlock();
        }
    }

    public void pong() throws InterruptedException {
        lock.lock();
        try {
            while(str == "pong")
                pinged.await();
            str = "pong";
            ponged.signal();
            System.out.println(str);
        } finally {
            lock.unlock();
        }
    }
}

class PingerLock implements Runnable{
    Thread t;
    private PingPongLock ppl;

    public PingerLock(PingPongLock ppl) {
        this.ppl = ppl;
        t= new Thread(this, "Ping");
        t.start();
    }

    public void run() {
        while(true)
            try {
                ppl.ping();
            } catch (InterruptedException e) {
                System.out.println(t.getName() + " interrupted.");
            }
    }
}

class PongerLock implements Runnable {
    Thread t;
    private PingPongLock ppl;

    public PongerLock(PingPongLock ppl) {
        this.ppl = ppl;
        t= new Thread(this, "Pong");
        t.start();
    }

    public void run() {
        while(true) {
            try {
                ppl.pong();
            } catch (InterruptedException e) {
                System.out.println(t.getName() + " interrupted.");
            }
        }
    }
}

public class InfinityPingPongWithLock {
    public static void main(String[] args) {
        PingPongLock ppl = new PingPongLock();
        PingerLock ping = new PingerLock(ppl);
        PongerLock pong = new PongerLock(ppl);
        try {
            ping.t.join();
            pong.t.join();
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }
}
