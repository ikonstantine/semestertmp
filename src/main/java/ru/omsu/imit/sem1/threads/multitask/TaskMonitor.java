package ru.omsu.imit.sem1.threads.multitask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class TaskMonitor implements Runnable {
    private CountDownLatch cdl;
    private BlockingQueue<Task> queue;
    private int readers;

    public TaskMonitor(BlockingQueue<Task> queue, CountDownLatch cdl, int readers) {
        this.cdl = cdl;
        this.queue = queue;
        this.readers = readers;
    }

    @Override
    public void run() {
        try {
            cdl.await();
            for (int i = 0; i < readers; i++)
                queue.add(new Task(-1));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
