package ru.omsu.imit.sem1.threads;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

class Formatter implements Runnable {
    private ThreadLocal<SimpleDateFormat> formatHolder = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy mm:HH");
        }
    };

    public void format(Date date) {
        System.out.println("Thread " + Thread.currentThread().getId() + "; format: " + formatHolder.get().format(date));
    }

    @Override
    public void run() {
        format(new Date());
        try {
            Thread.sleep(new Random().nextInt(1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        formatHolder.set(new SimpleDateFormat("dd/MM/yyyy mm:HH"));
        format(new Date());
    }
}

public class FormatterDemo {
    public static void main(String[] args) {
        Formatter fmt = new Formatter();
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(fmt);
            try {
                Thread.sleep(new Random().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t.start();
        }
    }
}
