package ru.omsu.imit.sem1.threads.multitask;

import java.util.concurrent.BlockingQueue;

public class TaskConsumer implements Runnable {
    private BlockingQueue<Task> queue;

    public TaskConsumer(BlockingQueue<Task> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Task task = queue.take();
                if (task.getValue() == -1)
                    break;
                task.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}