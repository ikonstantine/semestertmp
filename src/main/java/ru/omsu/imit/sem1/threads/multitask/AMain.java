package ru.omsu.imit.sem1.threads.multitask;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.concurrent.*;

class SetCommand implements Runnable {
    @Parameter(names = {"-ca"}, arity = 1, description = "Amount of thread-consumer")
    int amountConsumer;

    @Parameter(names = {"-pa"}, arity = 1, description = "Amount of thread-producer")
    int amountProducer;


    public void run() {
        BlockingQueue<MultiTask> queue = new ArrayBlockingQueue<>(100);
        CountDownLatch cdl = new CountDownLatch(10);
        MultiTaskMonitor mtm = new MultiTaskMonitor(queue, cdl, amountConsumer);
        
        ExecutorService executorService1 = Executors.newFixedThreadPool(5);
        for (int i = 0; i < amountConsumer; i++)
            executorService1.submit(new MultiTaskConsumer(queue, cdl));
        executorService1.shutdown();

        ExecutorService executorService2 = Executors.newFixedThreadPool(5);
        for (int i = 0; i < amountProducer; i++)
            executorService2.submit(new MultiTaskProducer(queue));
        executorService2.shutdown();

        mtm.run();
    }
}

public class AMain {
    public static void main(String[] args) {
        args = new String[] {"-ca", "10", "-pa", "10"};
        SetCommand sc = new SetCommand();
        JCommander.newBuilder().addObject(sc).build().parse(args);
        sc.run();
    }
}
