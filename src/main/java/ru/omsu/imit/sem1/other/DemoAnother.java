package ru.omsu.imit.sem1.other;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class DemoAnother {

    public static void renameFilenameExtension(String path) {
        File dir = new File(path);
        File[] arrayOfFiles = dir.listFiles();
        for (File file : arrayOfFiles) {
            if (file.isFile()) {
                String fName = file.getName();
                if (fName.substring(fName.length() - 4, fName.length()).equals(".dat"))
                    file.renameTo(new File(path+ "/" +fName.substring(0, fName.length() - 4) + ".bin"));
            }
        }
    }

    public static void writeNumbersByMappedByteBuffer(int size, String path) {
        try (FileChannel channel = new RandomAccessFile(new File(path), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, size);
            for (byte i = 0; i < size; i++)
                buffer.put(i);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))))) {
            for (int i = 0; i < size; i++)
                System.out.println(br.read());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
