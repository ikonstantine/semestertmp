package ru.omsu.imit.sem1.group;

import ru.omsu.imit.sem1.trainee.Trainee;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Constantine on 13.01.2018.
 */
public class GroupProcessor {
    public static void sortByName(Group group) {
        Collections.sort(group.getTrainees());
    }

    public static void sortByRating(Group group) {
        Collections.sort(group.getTrainees(), new Comparator<Trainee>() {
            @Override
            public int compare(Trainee t1, Trainee t2) {
                return t1.getRating() - t2.getRating();
            }
        });
    }

    public static boolean seekTraineeByName(Group group, String firstName, String lastName) {
        for (Trainee trainee : group.getTrainees())
            if (trainee.getFirstName().equals(firstName) && trainee.getLastName().equals(lastName))
                return true;
        return false;
    }
}