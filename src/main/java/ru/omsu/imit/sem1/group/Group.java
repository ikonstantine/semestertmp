package ru.omsu.imit.sem1.group;

import ru.omsu.imit.sem1.trainee.Trainee;

import java.util.ArrayList;
import java.util.List;


public class Group {

    private String name;
    List<Trainee> trainees;

    public Group(String name, List<Trainee> trainees) throws GroupException {
        setName(name);
        setTrainees(trainees);
    }

    public String getName() {
        return name;
    }

    public List<Trainee> getTrainees() {
        return trainees;
    }

    public void setName(String name) throws GroupException {
        if (name != null && !name.equals("") && !name.equals(" ")) this.name = name;
        else throw new GroupException(GroupErrorCodes.GROUP_ERROR_NAME);
    }

    public void setTrainees(List<Trainee> trainees) throws GroupException{
        if (trainees != null && trainees.size() > 0) {
            this.trainees = new ArrayList<>();
            this.trainees.addAll(trainees.subList(0, trainees.size()));
        } else throw new GroupException(GroupErrorCodes.GROUP_ERROR_LIST);
    }
}
