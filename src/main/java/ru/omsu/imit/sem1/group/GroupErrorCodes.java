package ru.omsu.imit.sem1.group;

public enum GroupErrorCodes {
    GROUP_ERROR_NAME(0, "name of group must not be null or empty"),
    GROUP_ERROR_LIST(1, "list of trainees must not be null or empty");

    private final int code;
    private final String reason;

    GroupErrorCodes(int code, String reason) {
        this.code = code;
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ERROR(" + code + "): " + reason;
    }
}
