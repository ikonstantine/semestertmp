package ru.omsu.imit.sem1.group;

public class GroupException extends Exception {
    public GroupException() {
        super();
    }

    public GroupException(String str) {
        super(str);
    }

    public GroupException(GroupErrorCodes gec) {
        super(gec.toString());
    }
}
