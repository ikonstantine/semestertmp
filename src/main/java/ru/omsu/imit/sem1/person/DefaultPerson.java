package ru.omsu.imit.sem1.person;

public class DefaultPerson {
    private DefaultPerson father;
    private DefaultPerson mother;

    public DefaultPerson() {
        super();
    }

    public DefaultPerson(DefaultPerson father, DefaultPerson mother) {
        super();
        this.father = father;
        this.mother = mother;
    }

    public DefaultPerson getFather() {
        return father;
    }

    public DefaultPerson getMother() {
        return mother;
    }

    public void setFather(DefaultPerson father) {
        this.father = father;
    }

    public void setMother(DefaultPerson mother) {
        this.mother = mother;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((father == null) ? 0 : father.hashCode());
        result = prime * result + ((mother == null) ? 0 : mother.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DefaultPerson other = (DefaultPerson) obj;
        if (father == null) {
            if (other.father != null)
                return false;
        } else if (!father.equals(other.father))
            return false;
        if (mother == null) {
            if (other.mother != null)
                return false;
        } else if (!mother.equals(other.mother))
            return false;
        return true;
    }

}
