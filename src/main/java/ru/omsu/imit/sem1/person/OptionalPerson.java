package ru.omsu.imit.sem1.person;
import java.util.Optional;

public class OptionalPerson {
    private Optional<OptionalPerson> father;
    private Optional<OptionalPerson> mother;

    public OptionalPerson(OptionalPerson father, OptionalPerson mother) {
        super();
        this.father = Optional.of(father);
        this.mother = Optional.of(mother);
    }

    public OptionalPerson() {
        super();
        this.father = Optional.empty();
        this.mother = Optional.empty();
    }

    public Optional<OptionalPerson> getFather() {
        return father;
    }

    public Optional<OptionalPerson> getMother() {
        return mother;
    }

    public void setFather(OptionalPerson father) {
        this.father = Optional.of(father);
    }

    public void setMother(OptionalPerson mother) {
        this.mother = Optional.of(mother);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((father == null) ? 0 : father.hashCode());
        result = prime * result + ((mother == null) ? 0 : mother.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OptionalPerson other = (OptionalPerson) obj;
        if (father == null) {
            if (other.father != null)
                return false;
        } else if (!father.equals(other.father))
            return false;
        if (mother == null) {
            if (other.mother != null)
                return false;
        } else if (!mother.equals(other.mother))
            return false;
        return true;
    }


}
