package ru.omsu.imit.sem1.group;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.collections.CollectionUtils;
import ru.omsu.imit.sem1.trainee.Trainee;
import ru.omsu.imit.sem1.trainee.TraineeException;

import java.util.*;

/**
 * Created by Constantine on 12.01.2018.
 */
public class GroupIsTest {

    @Test (expectedExceptions = GroupException.class)
    public void testOfConstructionOfGroup()throws GroupException {
        Group group = new Group("МПБ-503", new ArrayList<>());
    }

    @Test
    public void testOfSortByName() throws GroupException, TraineeException {
        List<Trainee> trainees = new ArrayList<>();
        trainees.add(new Trainee("Adam", "Ford", 5));
        trainees.add(new Trainee("Henry", "Mokko", 3));
        trainees.add(new Trainee("John", "Adapter", 4));
        trainees.add(new Trainee("Mike", "Berry", 5));

        Group group = new Group("МПБ-503", trainees);
        GroupProcessor.sortByName(group);
    }

    @Test
    public void testOfSortByRating() throws GroupException, TraineeException {
        List<Trainee> trainees = new ArrayList<>();
        trainees.add(new Trainee("Adam", "Ford", 5));
        trainees.add(new Trainee("Henry", "Mokko", 3));
        trainees.add(new Trainee("John", "Adapter", 4));
        trainees.add(new Trainee("Mike", "Berry", 1));

        Group group = new Group("МПБ-503", trainees);
        GroupProcessor.sortByRating(group);
        System.out.println(group.getTrainees());
    }

    @Test
    public void seekTraineeByName() throws GroupException, TraineeException{
        List<Trainee> trainees = new ArrayList<>();
        trainees.add(new Trainee("Adam", "Ford", 5));
        trainees.add(new Trainee("Henry", "Mokko", 3));
        trainees.add(new Trainee("John", "Adapter", 4));
        trainees.add(new Trainee("Mike", "Berry", 1));

        Group group = new Group("МПБ-503", trainees);
        Assert.assertTrue(GroupProcessor.seekTraineeByName(group, "Adam", "Ford"));
    }
}
