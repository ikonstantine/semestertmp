package ru.omsu.imit.sem1.java8;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;
import ru.omsu.imit.sem1.person.DefaultPerson;
import ru.omsu.imit.sem1.person.OptionalPerson;
import ru.omsu.imit.sem1.person.Person;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestOfJava8 {
    @Test
    public void splitCountTest() {
        Function<String, List<String>> split = string -> {
            List<String> list = new ArrayList<>();
            for (String subString : string.split(" "))
                list.add(subString);
            return list;
        };
        Function<List<?>, Integer> count = list -> list.size();
        Function<String, Integer> splitAndCount = s -> split.andThen(count).apply(s);
        Function<String, Integer> anotherSplitAndCount = s -> count.compose(split).apply(s);

        String string = "a b c d e f";
        List<String> res = split.apply(string);

        assertTrue(count.apply(res) == 6);
        assertTrue(splitAndCount.apply(string) == 6);
        assertTrue(anotherSplitAndCount.apply(string) == 6);
    }

    @Test
    public void functionTest() {
        Numbers num = new Numbers();
        assertEquals(66, num.get(65, 66, Math::max));
        assertEquals(13, num.get(13, -4, Math::max));
        assertEquals(-34, num.get(-34, -1, Math::min));

        Predicate<Integer> isEven = x -> x % 2 == 0;
        assertTrue(isEven.test(2));
        assertFalse(isEven.test(1));
        assertTrue(isEven.test(0));

        BiPredicate<Integer, Integer> areEqual = (x, y) -> x == y;
        assertTrue(areEqual.test(13, 13));
        assertFalse(areEqual.test(13, 1));

        Supplier<Date> getCurrentDate = () -> new Date();
        System.out.println(getCurrentDate.get());

        Function<String, Person> create = string -> new Person(string, 8);
        String name = "Anton";
        assertEquals(name, create.apply(name).getName());

        Personable personable = Person::new;
        assertEquals(name, personable.create(name, 8).getName());
    }

    @Test
    public void optionalTest() {
        Function<DefaultPerson, DefaultPerson> getMothersMotherFatherDefault = child -> {
            DefaultPerson temp = child.getMother();
            if (temp != null) {
                temp = temp.getMother();
                if (temp != null) {
                    temp = temp.getFather();
                    if (temp != null)
                        return temp;
                }
            }
            return null;
        };

        DefaultPerson dperson1 = new DefaultPerson();
        DefaultPerson dperson2 = new DefaultPerson();
        DefaultPerson dperson3 = new DefaultPerson(dperson1, dperson2);
        DefaultPerson dperson4 = new DefaultPerson();
        DefaultPerson dperson5 = new DefaultPerson();
        DefaultPerson dperson6 = new DefaultPerson();
        DefaultPerson dperson7 = new DefaultPerson(dperson4, dperson3);
        DefaultPerson dperson8 = new DefaultPerson(dperson6, dperson5);
        DefaultPerson dperson9 = new DefaultPerson(dperson8, dperson7);
        assertEquals(dperson1, getMothersMotherFatherDefault.apply(dperson9));
        assertEquals(null, getMothersMotherFatherDefault.apply(dperson8));



        Function<OptionalPerson, Optional<OptionalPerson>> getMothersMotherFather = child ->
                (Optional<OptionalPerson>) Optional.of(child).flatMap(OptionalPerson::getMother)
                    .flatMap(OptionalPerson::getMother).flatMap(OptionalPerson::getFather);

        OptionalPerson person1 = new OptionalPerson();
        OptionalPerson person2 = new OptionalPerson();
        OptionalPerson person3 = new OptionalPerson(person1, person2);
        OptionalPerson person4 = new OptionalPerson();
        OptionalPerson person5 = new OptionalPerson();
        OptionalPerson person6 = new OptionalPerson();
        OptionalPerson person7 = new OptionalPerson(person4, person3);
        OptionalPerson person8 = new OptionalPerson(person6, person5);
        OptionalPerson person9 = new OptionalPerson(person8, person7);

        assertEquals(Optional.of(person1), getMothersMotherFather.apply(person9));
        assertEquals(Optional.empty(), getMothersMotherFather.apply(person3));
    }

    IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    @Test
    public void intStreamTest() {
        IntStream stream = IntStream.rangeClosed(1, 5);
        int[] expectedArr = new int[] { 2, 4, 6, 8, 10 };
        int[] resultArr = transform(stream, x -> x + x).toArray();
        assertTrue(Arrays.equals(resultArr, expectedArr));
    }

    @Test
    public void PersonTest() {
        Predicate<Person> agePredicate = person -> person.getAge() > 30;
        Function<Person, String> nameFunction = person -> person.getName();
        List<Person> list = new ArrayList<>();
        list.add(new Person("Anton", 31));
        list.add(new Person("Viktor", 25));
        list.add(new Person("Vlad", 30));
        list.add(new Person("Louis", 35));
        list.add(new Person("Andrew", 36));
        list.add(new Person("Louis", 37));
        list.add(new Person("Kostya", 35));
        list.add(new Person("Kostya", 32));
        list.add(new Person("Kostya", 40));

        Stream<String> result = list.stream().filter(agePredicate).map(nameFunction).distinct()
                .sorted(Comparator.comparingInt(String::length));
        result.forEach(System.out::println); //Anton, Louis, Andrew, Kostya

        System.out.println();
        Stream<String> sorted = list.stream().filter(agePredicate).map(nameFunction)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map((a) -> a.getKey());
        sorted.forEach(System.out::println); //Anton(1), Andrew(1), Louis(2), Kostya(3)
    }

    @Test
    public void reduceTest() {
        List<Integer> integerList = new ArrayList<>();
        for (int i = 1; i < 10; i++)
            integerList.add(i);

        Optional<Integer> sum = integerList.stream().reduce((a, b) -> a + b);
        assertEquals(Optional.of(45), sum);

        Optional<Integer> product = integerList.stream().reduce((a, b) -> a * b);
        assertEquals(Optional.of(362880), product);
    }
}

class Numbers {
    public int get(int x, int y, IntBinaryOperator intBinaryOperator) {
        return intBinaryOperator.applyAsInt(x, y);
    }
}

interface Personable {
    Person create(String name, int age);
}