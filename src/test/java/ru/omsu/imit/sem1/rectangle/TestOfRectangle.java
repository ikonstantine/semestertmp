package ru.omsu.imit.sem1.rectangle;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestOfRectangle extends Assert {
    @Test
    public void testOfCreateRectangle() {
        Rectangle A = new Rectangle();
        System.out.println(A.toString());
    }

    @Test
    public void testOfWriteRectangleToBinaryToFile() {
        Rectangle rectangle = new Rectangle(45, 34, 23, 678);
        ProcessOfRectangle.writeToBinaryFile(rectangle, "rectangle_1.txt");
        Rectangle readRecstangle = ProcessOfRectangle.readFromBinaryFile("rectangle_1.txt");
        assertTrue(rectangle.equals(readRecstangle));
    }
}
