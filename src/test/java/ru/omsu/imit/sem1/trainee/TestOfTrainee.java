package ru.omsu.imit.sem1.trainee;

import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class TestOfTrainee {
    @Test (expectedExceptions = {TraineeException.class})
    public void testOfCatchTraineeExceptionForConstructor() throws TraineeException {
        Trainee inValidTrainee = new Trainee("Anton", "Zaycev", 7);
    }

    @Test (expectedExceptions = {TraineeException.class})
    public void testOfCatchTraineeExceptionForSetter() throws TraineeException {
        Trainee inValidTrainee = new Trainee("Anton", "Zaycev", 5);
        inValidTrainee.setFirstName("");
    }

    @Test
    public void testOfWrittingTraineeToFile() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Антон", "Зайцев", 5);
        TraineeProcessor.writeTrainee("student.txt", TraineeProcessor.contentInThreeLines(trainee));
    }

    @Test
    public void testOfReadTraineeFromFile() throws TraineeException {
        Trainee emptyTrainee = new Trainee();
        Trainee trainee = TraineeProcessor.readTraineeFromFile("student.txt");
        assertFalse(emptyTrainee.equals(trainee));
    }

    @Test
    public void testOfReadTraineeFromOneLine() throws TraineeException, IOException {
        Trainee emptyTrainee = new Trainee();
        Trainee trainee = TraineeProcessor.readTraineeByRegEx("student1.txt");
        assertFalse(emptyTrainee.equals(trainee));
    }

    @Test
    public void testOfSerializationTraineeToFileAndDeserialization() throws TraineeException, IOException, ClassNotFoundException{
        Trainee trainee = new Trainee("Антон", "Зайцев", 5);
        TraineeProcessor.serializeTrainee(trainee, "trainee_serialize.txt");
        Trainee checkTrainee = TraineeProcessor.deserializeToTrainee("trainee_serialize.txt");
        assertTrue(trainee.equals(checkTrainee));
    }

    /*@Test
    public void testOfSerializationAndDeserializationFromByteStream() throws TraineeException, ClassNotFoundException{
        Trainee trainee = new Trainee("Петр", "Сергеев", 5);
        Trainee newTrainee = TraineeProcessor.serializeToByteArrayOSAndDeserializeFromByteArrayIS(trainee);
        assertTrue(trainee.equals(newTrainee));
    }*/

    @Test
    public void testOfSerializationToGsonAndDeriazlizationToTrainee() throws TraineeException {
        Trainee trainee = new Trainee("Виктор", "Вдовиченко", 5);
        Trainee newTrainee = TraineeProcessor.serializeToGsonAndDeserializeToTrainee(trainee);
        assertTrue(trainee.equals(newTrainee));
    }

    @Test
    public void testOfSerializationToJsonFileAndDeserializationToFile() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Артем", "Клевакин", 5);
        TraineeProcessor.serializeToGsonFile(trainee, "student.json");
        Trainee newTrainee = TraineeProcessor.deserializeFromJsonFileToTrainee("student.json");
        assertTrue(trainee.equals(newTrainee));
    }

    @Test
    public void testOfReadBytesByMappedBuffer() throws TraineeException, IOException {
        Trainee t = new Trainee("Alexey", "Rodionov", 5);
        TraineeProcessor.writeBytesToFile("ar.txt", t);
        Trainee newT = TraineeProcessor.readBytesByMappedByteBuffer("ar.txt");
        assertTrue(t.equals(newT));
    }
}
