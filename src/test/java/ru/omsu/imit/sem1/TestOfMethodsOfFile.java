package ru.omsu.imit.sem1;

import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class TestOfMethodsOfFile {
    @Test
    public void testOfSome() throws IOException{
        File file1 = new File("C://SomeDir/test.txt");
        System.out.println(file1.createNewFile());
        System.out.println(file1.getName());
        System.out.println(file1.getAbsolutePath());
        System.out.println(file1.exists());
        //file1.delete();
        //System.out.println(file1.exists());
        File dir = new File("C://SomeDir");
        File newDir = new File("C://SomeDir/testRenamed.txt");
        System.out.println(file1.renameTo(newDir));
        if (file1.exists()) System.out.println("it exists");
        else System.out.println("it is not exists");
        if (file1.isDirectory()) System.out.println("it is directory");
        else if (file1.isFile()) System.out.println("it is file");
        System.out.println(Arrays.toString(dir.list()));
    }
}
